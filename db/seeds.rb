# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

phd = PlaceType.create(code: "PHD", description: "Provincial")
od = PlaceType.create(code: "OD", description: "Operational District")
hc = PlaceType.create(code: "HC", description: "Health Center")

# Phnom Penh
pp = Place.create(name: "Phnom Penh", place_type_id: phd.id)

# Lavea Em OD
levea_em_od = Place.create(name: "Lavea Em OD", place_type_id: od.id, parent_id: pp.id)

Place.create(name: "Areiy Ksat Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Barong Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Koh Keo Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Koh Reah Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Levea Sor Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Peam Okha Ong Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Phum Thom Soksabay Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Prek Rai Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Prek Russei Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Sarika Keo Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)
Place.create(name: "Toeuk Klaing Health Center", place_type_id: hc.id, parent_id: levea_em_od.id)

# Kean Svay OD
kean_svay_od = Place.create(name: "Kean Svay OD", place_type_id: od.id, parent_id: pp.id)

Place.create(name: "Banteay Dek Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Dei Eth Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "K'am Samnar Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Kampong Phnom Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Kean Svay Referral Hospital", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Korki Thom Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Phum Thom Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Prek Dach Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Prek Tonlorb Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Samrong Thom Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)
Place.create(name: "Sandar Health Center", place_type_id: hc.id, parent_id: kean_svay_od.id)

# admin user
User.create(name: "admin", password: "admin@instedd.org", place_id: pp.id, can_login: true)