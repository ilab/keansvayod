class AddCanLoginToUsers < ActiveRecord::Migration
  def up
    add_column :users, :can_login, :boolean, :default => false
  end

  def down
    remove_column :users, :can_login
  end
end
