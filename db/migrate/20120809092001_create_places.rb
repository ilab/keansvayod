class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.references :place_type
      t.integer :parent_id

      t.timestamps
    end
    add_index :places, :place_type_id
  end
end
