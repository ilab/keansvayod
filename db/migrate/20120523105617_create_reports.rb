class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :sender
      t.string :disease
      t.integer :quantity
      t.string :from
      t.string :lat
      t.string :lon
      t.string :to
      t.datetime :created_at
    end
  end
end
