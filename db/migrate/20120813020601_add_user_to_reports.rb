class AddUserToReports < ActiveRecord::Migration
  def change
    add_column :reports, :user_id, :integer, :default => nil
  end
end
