class AddPhoneNumberToUsers < ActiveRecord::Migration
  def up
    add_column :users, :phone_number, :string, :default => nil
  end

  def down
    remove_column :users, :phone_number
  end
end
