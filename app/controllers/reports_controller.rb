require "csv"
class ReportsController < ApplicationController
  def index
    unless current_user
      return redirect_to root_url
    end

    report_by_changed = false
    if params[:previous_view].present?
      if params[:previous_view] != "Weekly" and params[:report_by] == "Weekly"
        report_by_changed = true
      elsif params[:previous_view] == "Yearly" and params[:report_by] == "Monthly"
        report_by_changed = true
      end
    end

    date = (params[:from].present? and !report_by_changed) ? Date.strptime(params[:from], "%Y-%m-%d") : Time.new.to_date

    @previous_view = params[:report_by]
    @report_by = params[:report_by].present? ? params[:report_by] : "Weekly"

    date_range = date.to_range(@report_by)
    @week_number = date_range.number if date_range.kind_of? Week
    @from_date = date_range.from_date
    @to_date = date_range.to_date

    @places = current_user.place.children.all
    @diseases = Disease.all
    @reports = Report.joins(:place).between(@from_date, @to_date + 1.days).child_place_of(current_user.place).group("places.id", "reports.disease").sum(:quantity)
  end

  def create
    report_cases = RWCaseParser.parse request.raw_post
    user = User.find_by_phone_number_of_reporting_wheel params[:from]

    if !user.nil? and user.hc?
      Report.handle_update_and_create user, report_cases, params
    else
      raise Exception.new("User does not exist!")
    end

  end

  # GET /reports/export_csv
  # params :from, :to
  def export_csv
    from_date = Date.strptime(params[:from], "%Y-%m-%d") if params[:from].present?
    to_date = Date.strptime(params[:to], "%Y-%m-%d") if params[:to].present?
    reports = Report.joins(:place).between(from_date, to_date + 1.days).child_place_of(current_user.place).group("places.id", "reports.disease").sum(:quantity)

    crosstab = Crosstab.generate current_user.place.children, Disease.all, reports

    # re-formating Date dd-mm-yyyy
    from_date = from_date.strftime("%d-%m-%Y")
    to_date = to_date.strftime("%d-%m-%Y")

    csv_string = CSV.generate do |csv|
      csv << crosstab[:headers]
      crosstab[:bodies].each do |body|
        csv << body
      end
      csv << crosstab[:footers]
    end

    send_data csv_string,
      :type => 'text/csv; charset=iso-8859-1; header=present',
      :disposition => "attachment; filename = #{from_date}_to_#{to_date}_zero_report.csv"
  end

end
