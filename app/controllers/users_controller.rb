class UsersController < ApplicationController
  # GET /users
  # params :page
  def index
    if current_user.place.nil?
      User.all.paginate(:page => params[:page], :per_page => 20)
    else
      @users = current_user.members.paginate(:page => params[:page], :per_page => 20)
    end
  end

  def new
    @user = User.new
    @places = current_user.place.children
  end

  def create
    @user = User.new params[:user]
    @user.can_login = true if current_user.provincial?
    if @user.save
      redirect_to users_url, notice: 'User was successfully created.'
    else
      render action: "new"
    end
  end

  def edit
    @user = User.find(params[:id])
    @places = current_user.place.children
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to users_url, notice: 'User was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to users_url, notice: 'User was successfully deleted.'
  end
end