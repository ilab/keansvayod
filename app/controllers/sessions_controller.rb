class SessionsController < ApplicationController
  def new
  end
  def create
    user = User.authenticate(params[:name], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to  reports_path
    else
      redirect_to root_url, :notice => "Invalid name or password"
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
