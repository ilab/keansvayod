class Report < ActiveRecord::Base
  attr_accessible :created_at, :disease, :from, :lat, :lon, :quantity, :sender, :to, :user_id, :place_id
  belongs_to :user
  belongs_to :place

  REPORT_TYPE = {:weekly => "Weekly", :monthly => "Monthly", :yearly => "Yearly"}

  def self.between from_date, to_date
    where(:created_at => from_date..to_date)
  end

  def self.child_place_of place
    joins(:place).where("places.parent_id" => place.id)
  end

  def self.from_disease disease
    where(:disease => disease)
  end

  def self.find_by_place user
    where(:place_id => user.place_id)
  end

  def self.find_by_disease_and_date(user, disease, from_date, to_date)
    records = Report.from_disease(disease).between(from_date, to_date).find_by_place(user)
    if records.count > 0
      records[0]
    else
      nil
    end
  end

  def self.find_no_disease report_cases
    report_cases.select{|disease, quantity| disease == "No_Disease"}.count
  end

  def self.handle_update_and_create user, report_cases, options
    date = Time.now.to_date
    date_range = date.to_range("Weekly")
    from_date = date_range.from_date
    to_date = date_range.to_date
    no_disease = Report.find_no_disease report_cases

    if no_disease > 0
      Report.update_all("quantity = 0", :created_at => from_date..to_date, :user_id => user.id)
    end
    report_cases.each do |disease, quantity|
      record = Report.find_by_disease_and_date(user, disease, from_date, to_date)
      if !record.nil?
        record.quantity = (no_disease > 0) ? 0 : quantity
        record.save
      else
        Report.create(:user_id => user.id, :sender => options[:sender],
          :disease => disease, :quantity => quantity, :from => options[:from],
          :lat => options[:lat], :lon => options[:lon], :to => options[:to], :place_id => user.place_id)
      end
    end
  end

end

