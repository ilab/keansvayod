class Month

  def initialize date
    @date = date
  end

  def from_date
    Date.new(@date.year, @date.mon, 1)
  end

  def to_date
    Date.new(@date.year, @date.mon, 1) +1.month - 1.days
  end

end