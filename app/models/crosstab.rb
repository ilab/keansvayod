class Crosstab
  def self.generate rows, columns, values

    # generate headers
    headers = []
    headers << "Health Center"
    columns.each do |column|
      headers << column[:name]
    end
    # generate body
    bodies = []
    rows.each do |row|
      body = []
      body << row[:name]
      columns.each do |column|
        row_column_value = values.collect {|k, v| v if k[0] == row[:id] and k[1] == column[:name]}.compact.first
        body << row_column_value
      end
      if body.select{|v| v = !v.nil?}.length > 1
        body = body.collect{|v| !v.nil? ? v : 0}
      end
      bodies << body
    end

    # generate footer/total
    footers = []
    footers << "Total:"
    columns.each do |column|
      total = 0
      values.each do |key, value|
        total += value if key[1] == column[:name]
      end
      footers << total
    end

    # hide No Disease case
    headers.pop
    bodies.collect{|v| v.pop}
    footers.pop

    crosstab = {:headers => headers, :bodies => bodies, :footers => footers}

  end
end