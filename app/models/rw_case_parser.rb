class RWCaseParser

  # Sample format post from mobile
  # raw_data = "Disease:\"Rab\", Value:\"1\" Disease:\"DPT\", Value:\"10\" Disease:\"ME\", Value:\"20\" Disease:\"DHF\", Value:\"19\""

  #  replace all Disease: and Value: with "", delete all "(double quote) and ,(comma), replace "  "(two spaces) with " "(single space), and split with " " 
  def self.parse text
    rw_cases = {}
    array = text.gsub(/(Disease:)|(Value:)|[",]/, "").split(" ").compact()
    for i in 1..(array.length()/2)
      rw_cases[array[2*i-2]] = array[2*i-1].to_i # {"AWD" => number}
    end
    rw_cases
  end
end