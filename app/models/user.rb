class User < ActiveRecord::Base
  belongs_to :place
  has_many :reports

  attr_accessible :name, :password, :place_id, :can_login, :phone_number

  validates :place_id, :presence => true
  validates :name, :presence => true
  validates :name, :uniqueness => true

  def self.authenticate(name, password)
    can_login?.where(:name => name, :password => password).first
  end

  def self.can_login?
    where(:can_login => true)
  end

  def members
    User.where(:place_id => self.place.children.select(:id))
  end

  def provincial?
    self.place && self.place.place_type.code == "PHD"
  end

  def od?
    self.place && self.place.place_type.code == "OD"
  end

  def hc?
    self.place && self.place.place_type.code == "HC"
  end

  def self.find_by_phone_number_of_reporting_wheel phone_number_rw
    sender_phone_number = phone_number_rw.gsub(/(sms:\/\/)/, "")
    User.find_by_phone_number sender_phone_number
  end

end
