Reportwheel::Application.routes.draw do
  
  resources :reports do
    collection do
      get :export_csv
    end
  end

  resources :users
  
  get "logout" => "sessions#destroy", :as => "logout"
  get "login" => "sessions#new", :as => "login"
  root :to => "sessions#new"

  resource :sessions
  
end